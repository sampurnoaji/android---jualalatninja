package com.example.minicommerce.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.minicommerce.R;

/**
 * Dibuat oleh petersam
 */
public class CardViewHolder extends RecyclerView.ViewHolder {
    ImageView img;
    TextView produk, harga, massa, tvQty;
    ImageView btnAdd, btnMin;
    RelativeLayout disable;
    CardView card;

    public CardViewHolder(@NonNull View itemView) {
        super(itemView);
        img = itemView.findViewById(R.id.card_image);
        produk = itemView.findViewById(R.id.card_product);
        massa = itemView.findViewById(R.id.card_massa);
        harga = itemView.findViewById(R.id.card_price);
        tvQty = itemView.findViewById(R.id.card_qty);
        btnAdd = itemView.findViewById(R.id.card_btn_add);
        btnMin = itemView.findViewById(R.id.card_btn_min);
        disable = itemView.findViewById(R.id.disable_stok);
        card = itemView.findViewById(R.id.card_jual);
    }
}
