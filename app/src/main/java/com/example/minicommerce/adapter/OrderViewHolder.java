package com.example.minicommerce.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.minicommerce.R;

/**
 * Dibuat oleh petersam
 */
public class OrderViewHolder extends RecyclerView.ViewHolder {
    TextView orderProduk, orderHarga, orderQty;
    CardView card;

    public OrderViewHolder(@NonNull View itemView) {
        super(itemView);
        orderProduk = itemView.findViewById(R.id.beli_weapon);
        orderHarga = itemView.findViewById(R.id.beli_harga);
        orderQty = itemView.findViewById(R.id.beli_qty);
        card = itemView.findViewById(R.id.beli_card);
    }
}
