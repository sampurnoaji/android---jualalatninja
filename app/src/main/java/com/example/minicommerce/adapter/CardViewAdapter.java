package com.example.minicommerce.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.minicommerce.R;
import com.example.minicommerce.data.Weapon;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

/**
 * Dibuat oleh petersam
 */
public class CardViewAdapter extends RecyclerView.Adapter<CardViewHolder> {
    private Context context;
    private ArrayList<Weapon> list;
    private TextView tvTotalHarga;

    public CardViewAdapter(Context context, ArrayList<Weapon> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(@NonNull final CardViewHolder holder, final int position) {
        final Weapon weapon;
        weapon = list.get(position);
        setupValueCard(holder, weapon, position);
        if (position == 2){ disableCard(holder); }
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setupImgDialog(weapon);
            }
        });
        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weapon.addQty();
                holder.tvQty.setText("" + weapon.getQty());
                setupTotalHarga();
            }
        });
        holder.btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weapon.minQty();
                holder.tvQty.setText("" + weapon.getQty());
                setupTotalHarga();
            }
        });
    }

    private void setupValueCard(CardViewHolder holder, Weapon weapon, int position){
        weapon.setId(position);
        holder.produk.setText(weapon.getProduk());
        holder.massa.setText(weapon.getMassa());
        holder.harga.setText(toRupiah(Long.valueOf(weapon.getHarga())));
        Glide.with(context).load(weapon.getImg()).into(holder.img);
    }

    private void setupTotalHarga() {
        tvTotalHarga = ((Activity)context).findViewById(R.id.tv_total_harga);
        long totalHarga = 0;
        for (int i = 0; i < list.size(); i++){
            totalHarga += Long.valueOf(list.get(i).getHarga()) * Long.valueOf(list.get(i).getQty());
        }
        tvTotalHarga.setText(toRupiah(totalHarga));
    }

    private void setupImgDialog(Weapon weapon){
        View view = LayoutInflater.from(context).inflate(R.layout.layout_image, null);
        ImageView imageView = view.findViewById(R.id.detail_img);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        Glide.with(context).load(weapon.getImg()).into(imageView);
        builder.create().show();
    }

    private String toRupiah(long uangLong){
        DecimalFormat kursIdr = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp ");
        formatRp.setGroupingSeparator('.');
        formatRp.setMonetaryDecimalSeparator(',');
        kursIdr.setDecimalFormatSymbols(formatRp);
        String uangStr = kursIdr.format(uangLong);
        return uangStr;
    }

    private void disableCard(CardViewHolder holder){
        holder.disable.setVisibility(View.VISIBLE);
        holder.img.setEnabled(false);
        holder.btnAdd.setEnabled(false);
        holder.btnMin.setEnabled(false);
    }

    public int getId(int position){
        Weapon weapon = list.get(position);
        return weapon.getId();
    }

    public String getProduk(int position){
        Weapon weapon = list.get(position);
        return weapon.getProduk();
    }

    public String getHarga(int position){
        Weapon weapon = list.get(position);
        return weapon.getHarga();
    }

    public int getQty(int position){
        Weapon weapon = list.get(position);
        return weapon.getQty();
    }
}