package com.example.minicommerce.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.minicommerce.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * Dibuat oleh petersam
 */
public class OrderViewAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    private Context context;
    private String[][] orderList;
    private Long totalHargaProduk;

    public OrderViewAdapter(Context context, String[][] orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public OrderViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_order, parent, false);
        return new OrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OrderViewHolder holder, final int position) {
        holder.orderProduk.setText(orderList[position][1]);
        totalHargaProduk = Long.valueOf(orderList[position][2]) * Long.valueOf(orderList[position][3]);
        holder.orderHarga.setText(toRupiah(totalHargaProduk));
        holder.orderQty.setText("x" + orderList[position][3]);
    }

    @Override
    public int getItemCount() {
        return orderList.length;
    }

    public String toRupiah(long uangLong){
        DecimalFormat kursIdr = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
        formatRp.setCurrencySymbol("Rp ");
        formatRp.setGroupingSeparator('.');
        formatRp.setMonetaryDecimalSeparator(',');
        kursIdr.setDecimalFormatSymbols(formatRp);
        String uangStr = kursIdr.format(uangLong);
        return uangStr;
    }
}
