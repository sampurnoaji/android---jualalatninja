package com.example.minicommerce;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minicommerce.adapter.CardViewAdapter;
import com.example.minicommerce.data.Weapon;
import com.example.minicommerce.data.WeaponData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ArrayList<Weapon> list;
    private String[][] orderList;
    private CardViewAdapter adapter;
    private CardView cardTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupToolbar();
        loadData();
        showData();
        setupIntent();
    }

    private void setupToolbar() {
        recyclerView = findViewById(R.id.rv);
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView title = findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText("Gaman Shop");
    }

    private void loadData() {
        list = new ArrayList<>();
        list.addAll(WeaponData.getListData());
    }

    private void showData() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new CardViewAdapter(this, list);
        recyclerView.setAdapter(adapter);
    }

    private void setupIntent() {
        cardTotal = findViewById(R.id.total_card);
        cardTotal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int panjang = 0;
                for (int i = 0; i < adapter.getItemCount(); i++) {
                    if (adapter.getQty(i) != 0){
                        panjang++;
                    }
                }
                orderList = new String[panjang][4];
                int count = 0;
                for (int i = 0; i < adapter.getItemCount(); i++){
                    if (adapter.getQty(i) != 0){
                        orderList[count][0] = String.valueOf(adapter.getId(i));
                        orderList[count][1] = adapter.getProduk(i);
                        orderList[count][2] = adapter.getHarga(i);
                        orderList[count][3] = String.valueOf(adapter.getQty(i));
                        count++;
                    }
                }
                Intent intent = new Intent(getApplicationContext(), BeliActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("orderList", orderList);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    public int getItemCount(){
        loadData();
        return list.size();
    }
}
