package com.example.minicommerce;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minicommerce.adapter.OrderViewAdapter;

public class BeliActivity extends AppCompatActivity {
    private TextView tvWeapon, tvHarga;
    private String[][] orderList;
    private RecyclerView recyclerView;
    private OrderViewAdapter adapter;
    private Toolbar toolbar;
    private TextView title;
    private TextView tvTotalHarga;
    private Button btnPlaceOrder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beli);
        setupView();
        getOrderList();
    }

    private void setupView() {
        toolbar = findViewById(R.id.toolbar);
        title = findViewById(R.id.toolbar_title);
        tvWeapon = findViewById(R.id.beli_weapon);
        tvHarga = findViewById(R.id.beli_harga);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        title.setText("Order Summary");

        recyclerView = findViewById(R.id.rv_order);
        tvTotalHarga = findViewById(R.id.tv_beli_total);
        btnPlaceOrder = findViewById(R.id.beli_place_order);
    }

    private void getOrderList(){
        Bundle bundle = getIntent().getExtras();
        orderList = (String[][]) bundle.getSerializable("orderList");

        recyclerView.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        adapter = new OrderViewAdapter(this, orderList);
        recyclerView.setAdapter(adapter);

        int harga;
        int totalHarga = 0;
        for (int i = 0; i < orderList.length; i++) {
            harga = Integer.valueOf(orderList[i][2]);
            totalHarga = totalHarga + harga;
        }
        tvTotalHarga.setText(adapter.toRupiah(totalHarga));

        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), EndActivity.class));
                finish();
            }
        });
    }
}
