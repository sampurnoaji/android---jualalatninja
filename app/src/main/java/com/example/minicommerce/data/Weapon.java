package com.example.minicommerce.data;

/**
 * Dibuat oleh petersam
 */
public class Weapon {
    private int id;
    private String produk, harga, img, massa;
    private int qty;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQty() {
        return qty;
    }

    public void addQty() {
        this.qty += 1;
    }

    public void minQty(){
        if (this.qty > 0){
            this.qty -= 1;
        }
    }

    public String getMassa() {
        return massa;
    }

    public void setMassa(String massa) {
        this.massa = massa;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
