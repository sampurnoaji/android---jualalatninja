package com.example.minicommerce.data;

import java.util.ArrayList;

/**
 * Dibuat oleh petersam
 */
public class WeaponData {
    static Weapon weapon;

    public static String[][] data = new String[][]{
            {"Tensa Zangetsu", "92000", "1400 g", "https://images-na.ssl-images-amazon.com/images/I/51Q90Wr%2BGqL.jpg"},
            {"Tanto Ninja Katana", "69000", "1250 g", "https://images-na.ssl-images-amazon.com/images/I/61psWSfW3AL._SL1000_.jpg"},
            {"Dark Assassin Dagger", "49000", "181 g", "https://images-na.ssl-images-amazon.com/images/I/41IKGQw%2BOvL.jpg"},
            {"Shuriken", "7800", "6.5 cm", "https://images-na.ssl-images-amazon.com/images/I/61eJJ%2BPqfIL._SL1500_.jpg"},
            {"Kunai", "25000", "6.5 inch", "https://images-na.ssl-images-amazon.com/images/I/61GAst-gdnL._SL1000_.jpg"},
            {"Kunai Sword", "16000", "22 cm", "https://images-na.ssl-images-amazon.com/images/I/61srixS8uIL._SL1024_.jpg"},
            {"Triple-Bladed Scythe", "150000", "3500 g", "https://images-na.ssl-images-amazon.com/images/I/51jfyxD3-DL._SL1024_.jpg"},
            {"Kubikiribouchou", "74000", "3200 g", "https://images-na.ssl-images-amazon.com/images/I/510j%2Be7KhHL._SL1500_.jpg"},
            {"Zabimaru", "119000", "2200 g", "https://images-na.ssl-images-amazon.com/images/I/51xfbVGQEfL._SL1500_.jpg"},
            {"Aqua", "3000", "600 ml", "https://indotirtaabadi.com/wp-content/uploads/2015/06/DanoneAqua.jpg"}
    };

    public static ArrayList<Weapon> getListData(){
        ArrayList<Weapon> list = new ArrayList<>();
        for (int i = 0; i < data.length; i++){
            weapon = new Weapon();
            weapon.setProduk(data[i][0]);
            weapon.setHarga(data[i][1]);
            weapon.setMassa(data[i][2]);
            weapon.setImg(data[i][3]);
            list.add(weapon);
        }
        return list;
    }
}
